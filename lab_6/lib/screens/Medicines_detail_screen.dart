import 'package:flutter/material.dart';

import '../dummy_data.dart';

class MedicineDetailScreen extends StatelessWidget {
  static const routeName = '/Medicine-detail';

  final Function toggleFavorite;
  final Function isFavorite;

  MedicineDetailScreen(this.toggleFavorite, this.isFavorite);

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final MedicineId = ModalRoute.of(context).settings.arguments as String;
    final selectedMedicine =
        DUMMY_Medicine.firstWhere((Medicine) => Medicine.id == MedicineId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedMedicine.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(children: <Widget>[
              Positioned(
                  child: Container(
                height: 400,
                width: double.infinity,
                child: Image(
                  image: AssetImage(selectedMedicine.imageurl),
                  fit: BoxFit.cover,
                ),
              )),
              Positioned(
                bottom: 0,
                width: 400,
                child: Container(
                  child: Text(
                    selectedMedicine.price,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 26),
                  ),
                ),
              ),
            ]),
            buildSectionTitle(context, 'Description'),
            buildContainer(
              ListView.builder(
                itemBuilder: (ctx, index) => Card(
                  color: Colors.white,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 10,
                      ),
                      child: Text(
                        selectedMedicine.description[index],
                        style: TextStyle(fontSize: 18),
                      )),
                ),
                itemCount: selectedMedicine.description.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
