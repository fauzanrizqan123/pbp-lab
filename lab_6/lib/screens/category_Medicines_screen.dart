import 'package:flutter/material.dart';

import '../widgets/Medicine_item.dart';
import '../models/Medicine.dart';

class CategoryMedicineScreen extends StatefulWidget {
  static const routeName = '/category-Medicine';

  final List<Medicine> availableMedicine;

  CategoryMedicineScreen(this.availableMedicine);

  @override
  _CategoryMedicineScreenState createState() => _CategoryMedicineScreenState();
}

class _CategoryMedicineScreenState extends State<CategoryMedicineScreen> {
  String categoryTitle;
  List<Medicine> displayedMedicine;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      displayedMedicine = widget.availableMedicine.where((Medicine) {
        return Medicine.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.14),
        ),
        itemBuilder: (ctx, index) {
          return MedicineItem(
            id: displayedMedicine[index].id,
            title: displayedMedicine[index].title,
            duration: displayedMedicine[index].price,
            imageurl: displayedMedicine[index].imageurl,
          );
        },
        itemCount: displayedMedicine.length,
      ),
    );
  }
}
