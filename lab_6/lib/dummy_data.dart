import 'package:flutter/material.dart';

import './models/category.dart';
import './models/Medicine.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Covid-19',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c2',
    title: 'Alergi',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c3',
    title: 'Herbal',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c4',
    title: 'Vitamin',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c5',
    title: 'Flu',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c6',
    title: 'Suplemen Makanan',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c7',
    title: 'Kebutuhan Ibu',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c8',
    title: 'Diabetes',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'c9',
    title: 'Persendian dan Otot',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
];

const DUMMY_Medicine = const [
  Medicine(
    id: 'm1',
    imageurl: 'images/1.png',
    categories: [
      'c1',
      'c4',
    ],
    title: 'Hi-D Vitamin D',
    price: 'Rp 20.000',
    description: [
      'HI-D 5000 IU mengandung cholecalciferol 5000 IU setara dengan Vitamin D3 125 mikrogram. Obat ini termasuk golongan vitamin D dan analog. HI-D 5000 digunakan untuk menaikkan kadar vitamin D pada pasien yang kekurangan vitamin D. Dalam penggunaan obat ini harus SESUAI DENGAN PETUNJUK DOKTER.'
    ],
  ),
  Medicine(
    id: 'm2',
    imageurl: 'images/2.png',
    categories: [
      'c1',
      'c4',
    ],
    title: 'Halowell C',
    price: 'Rp 13.000',
    description: [
      'HALOWELL C 500 MG TABLET merupakan suplemen berbentuk tablet yang mengandung Vitamin C. Suplemen ini digunakan untuk memenuhi kebutuhan harian vitamin C tubuh.'
    ],
  ),
  Medicine(
    id: 'm3',
    imageurl: 'images/3.png',
    categories: [
      'c1',
      'c2',
    ],
    title: 'NaCl',
    price: 'Rp 10.300',
    description: [
      'NACL 0.9 % merupakan cairan infus yang mengandung NaCl 0.9%. Infus ini digunakan untuk mengembalikan keseimbangan elektrolit pada dehidrasi. Ion natrium adalah elektrolit utama pada cairan ekstraselular yang diperlukan dalam distribusi cairan dan elektrolit lainnya. Ion klorida berperan sebagai buffering agen pada paru-paru dan jaringan.'
    ],
  ),
];
