import 'package:flutter_complete_guide/widgets/main_drawer.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<FormScreen> {
  String _name;
  String _NIK;
  String _phoneNumber;

  final List<Map<String, dynamic>> _items = [
    {
      'value': 'vaksin1',
      'label': 'Dokter Vaksin',
    },
    {
      'value': 'vaksin2',
      'label': 'In Harmony Clinic',
    },
    {
      'value': 'vaksin3',
      'label': 'Sentar Vaksin JIEP',
    },
  ];

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value.isEmpty) {
          return 'Insert your name!';
        }

        return null;
      },
      onSaved: (value) {
        _name = value;
      },
    );
  }

  Widget _buildNIK() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'NIK'),
      validator: (value) {
        if (value.isEmpty) {
          return 'Insert your NIK!';
        }

        return null;
      },
      onSaved: (value) {
        _NIK = value;
      },
    );
  }

  Widget _buildVacPlace() {
    return SelectFormField(
      type: SelectFormFieldType.dropdown, // or can be dialog
      labelText: 'Vaccination Place',
      items: _items,
      validator: (value) {
        if (value.isEmpty) {
          return 'Insert your NIK!';
        }

        return null;
      },
      onChanged: (val) => print(val),
      onSaved: (val) => print(val),
    );
  }

  Widget _buildphoneNumber() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Phone Number'),
      validator: (value) {
        if (value.isEmpty) {
          return 'Insert your date of birth!';
        }

        return null;
      },
      onSaved: (value) {
        _phoneNumber = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Register a Vaccine")),
        drawer: MainDrawer(),
        body: Container(
            margin: EdgeInsets.all(24),
            child: Form(
                key: _formKey,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildName(),
                      _buildphoneNumber(),
                      _buildVacPlace(),
                      _buildNIK(),
                      SizedBox(height: 100),
                      RaisedButton(
                          child: Text('Submit',
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 18)),
                          onPressed: () => {
                                if (_formKey.currentState.validate())
                                  {
                                    _formKey.currentState.save(),
                                    showDialog<String>(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                              title: const Text(
                                                  'Successfully registered!'),
                                              content: Text(
                                                  'The following user:\nname: $_name\nNIK: $_NIK\nPhone Number: $_phoneNumber\nhas been registered!'),
                                              actions: [
                                                TextButton(
                                                    onPressed: () =>
                                                        Navigator.pop(
                                                            context, 'OK'),
                                                    child: const Text('OK'))
                                              ],
                                            ))
                                  },
                              })
                    ]))));
  }
}
