import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  static const routeName = '/';

  final Function saveFilters;
  final Map<String, bool> currentFilters;

  FiltersScreen(this.currentFilters, this.saveFilters);

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('HeyDoc'),
        ),
        drawer: MainDrawer(),
        body: Center(
            child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text:
                          'Welcome to HeyDoc\n - Your personal Covid-19 assistant -\n\n',
                      style: Theme.of(context).textTheme.headline6,
                      children: const <TextSpan>[
                        TextSpan(
                            text:
                                'In this pandemic era, HeyDoc is a platform to assist you.\nWith our assistance, we strive to make your daily life easier through this pandemic\n\n\n',
                            style: TextStyle(
                                fontWeight: FontWeight.normal, fontSize: 26)),
                        TextSpan(
                            text: "Indonesia's Covid - 19 Cases Right Now:\n",
                            style: TextStyle(
                                fontWeight: FontWeight.normal, fontSize: 20)),
                        TextSpan(
                            text:
                                'Positive : 4250855\nHospitalized : 9018\nDeath Cases : 143659\nHealed : 4098178\n\n',
                            style: TextStyle(
                                fontWeight: FontWeight.normal, fontSize: 18)),
                        TextSpan(
                            text: "Our Services",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 36)),
                      ],
                    ),
                  )),
              Row(
                children: const <Widget>[
                  Expanded(
                    child: Image(
                        image: AssetImage('images/medicine.png'),
                        width: 200,
                        height: 200),
                  ),
                  Expanded(
                    child: Image(
                        image: AssetImage('images/info.png'),
                        width: 200,
                        height: 200),
                  ),
                  Expanded(
                    child: Image(
                        image: AssetImage('images/stethoscope.png'),
                        width: 200,
                        height: 200),
                  ),
                  Expanded(
                    child: Image(
                        image: AssetImage('images/syringe.png'),
                        width: 200,
                        height: 200),
                  )
                ],
              )
            ],
          ),
        )));
  }
}
