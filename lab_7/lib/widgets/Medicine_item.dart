import 'package:flutter/material.dart';

import '../screens/Medicines_detail_screen.dart';
import '../models/Medicine.dart';

class MedicineItem extends StatelessWidget {
  final String id;
  final String title;
  final String duration;
  final String imageurl;

  MedicineItem({
    @required this.id,
    @required this.title,
    @required this.duration,
    @required this.imageurl,
  });

  void selectMedicine(BuildContext context) {
    Navigator.of(context)
        .pushNamed(
      MedicineDetailScreen.routeName,
      arguments: id,
    )
        .then((result) {
      if (result != null) {
        // removeItem(result);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectMedicine(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(children: <Widget>[
              Expanded(
                child:
                    Image(image: AssetImage(imageurl), width: 200, height: 200),
              )
            ]),
            Padding(
                padding: EdgeInsets.all(20),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: '$title\n$duration',
                    style: TextStyle(fontSize: 16),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
