import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/Medicines_detail_screen.dart';
import './screens/category_Medicines_screen.dart';
import './screens/filters_screen.dart';
import './screens/categories_screen.dart';
import './screens/form_screen.dart';
import 'models/Medicine.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {};
  List<Medicine> _availableMedicines = DUMMY_Medicine;
  List<Medicine> _favoriteMedicines = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;
    });
  }

  void _toggleFavorite(String MedicineId) {
    final existingIndex =
        _favoriteMedicines.indexWhere((Medicine) => Medicine.id == MedicineId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteMedicines.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMedicines.add(
          DUMMY_Medicine.firstWhere((Medicine) => Medicine.id == MedicineId),
        );
      });
    }
  }

  bool _isMedicineFavorite(String id) {
    return _favoriteMedicines.any((Medicine) => Medicine.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HeyDoc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        canvasColor: Color.fromRGBO(238, 238, 238, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 36,
              fontFamily: 'Dosis',
              fontWeight: FontWeight.bold,
            ),
            headline1: TextStyle(
                color: Color.fromRGBO(238, 238, 238, 1),
                fontSize: 26,
                fontFamily: 'Dosis',
                fontWeight: FontWeight.bold)),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/medicine': (ctx) => TabsScreen(_favoriteMedicines),
        CategoryMedicineScreen.routeName: (ctx) =>
            CategoryMedicineScreen(_availableMedicines),
        MedicineDetailScreen.routeName: (ctx) =>
            MedicineDetailScreen(_toggleFavorite, _isMedicineFavorite),
        FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
        '/vac-info': (ctx) => FormScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => CategoriesScreen(),
        );
      },
    );
  }
}
