import 'package:flutter/foundation.dart';

class Medicine {
  final String id;
  final List<String> categories;
  final String title;
  final List<String> description;
  final String price;
  final String imageurl;

  const Medicine(
      {@required this.id,
      @required this.categories,
      @required this.title,
      @required this.description,
      @required this.price,
      @required this.imageurl});
}
